#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3

from conf.configs import *


class Db(object):
    '数据读写类'
    def __init__(self, db_path):
        self.db_path = db_path
        self.connect = sqlite3.connect(db_path)

    def initialize(self,sql):

        try:
            cursor = self.connect.cursor()
            with open(sql, 'r') as f:
                s = f.read()
                if s is not None:
                    cursor.executescript(s)
                else:
                    logging.info(r'文件不存在')

            cursor.close()
        except Exception as e:
            logging.error(e)

    def __del__(self):
        if self.connect is not None:
            self.connect.commit()
            self.connect.close()
