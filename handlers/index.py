#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys, asyncio
from aiohttp import web


def index(request):
    return web.Response(body=b'<h1>Awesome</h1>', headers={'Content-Type': 'text/html;utf-8'})