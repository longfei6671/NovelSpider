CREATE TABLE IF NOT EXISTS fiction
(
    fiction_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    fiction_name VARCHAR(300) NOT NULL,
    fiction_img_url VARCHAR(1000),
    fiction_summary VARCHAR(1000),
    create_time INTEGER
);
CREATE TABLE fiction_source
(
    fiction_source_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    fiction_id INTEGER NOT NULL,
    fiction_source_url VARCHAR(1000) NOT NULL,
    fiction_source_name VARCHAR(500),
    is_default INTEGER DEFAULT 0 NOT NULL
);
CREATE TABLE IF NOT EXISTS fiction_content
(
    fiction_content_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    fiction_source_id INTEGER NOT NULL,
    fiction_content_title VARCHAR(1000) NOT NULL,
    fiction_content_url VARCHAR(1000) NOT NULL,
    fiction_content_code VARCHAR(100) NOT NULL,
    fiction_content TEXT,
    create_time INT
);
