import os,logging.config

base_path = os.path.abspath('./../')

logging_config_path = os.path.join(base_path,'conf', 'logging.conf')

logging.config.fileConfig(logging_config_path)

logging.getLogger()

configs = {'db_path': os.path.join(base_path,'data','fiction.db'),'sql_path':os.path.join(base_path,'data','fiction.sql')}
